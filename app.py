from flask import Flask, render_template, request, redirect, jsonify
import requests
from bokeh.embed import components
from datetime import datetime
from bokeh.plotting import figure
from bokeh.resources import INLINE

app = Flask(__name__)
app.vars = {}

colors = {
    1: '#000000',
    2:   '#FF0000',
    3: '#00FF00',
    4:  '#0000FF',
}
def getitem(obj, item, default):
    if item not in obj:
        return default
    else:
        return obj[item]

@app.route('/')
def main():
  return redirect('/index')

@app.route('/index')
def index():

    # Grab the inputs arguments from the URL
    args = request.args

    r = requests.get('https://www.quandl.com/api/v3/datasets/WIKI/FB/data.json?api_key=W3NQ-kRJzLRvyr2k-BdD')
    data = r.json()['dataset_data']['data']

    chart = int(getitem(args, 'chart', 1))

    print("Selected chart: {}".format(chart))


    print(len(data))
    print([datetime.strptime(datum[0], '%Y-%m-%d') for datum in sorted(data[:10])])
    print([datum[chart] for datum in sorted(data[:10])])


    js_resources = INLINE.render_js()
    css_resources = INLINE.render_css()



    x = [datetime.strptime(datum[0], '%Y-%m-%d') for datum in sorted(data)]
    y = [datum[chart] for datum in sorted(data)]

    plot = figure(
                  title='Data from Quandle WIKI set',
                  x_axis_label='date',
                  x_axis_type='datetime')

    plot.line(x, y, color=colors[chart], line_width=2)


    script, div = components(plot)

    return render_template('graph.html',
                           plot_script=script,
                           plot_div=div,
                           js_resources=js_resources,
                           css_resources=css_resources,
                           chart = str(chart)
                           )


if __name__ == '__main__':
  app.run(port=33507)
  # app.run(port=33507, debug=True)
